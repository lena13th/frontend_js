/**
 * Created by lena1 on 23.07.2017.
 */
'use strict';

// 1. Конструктор Vehicle
function Vehicle(name, description) {
    // сохранение параметров name, description
    this.name = name;
    this.description = description;
}

// 2. Конструктор Truck
function Truck(name, description, carrying) {
    this.carrying = carrying;
    Vehicle.apply(this, [name, description]);

    // информация для отображения в списке машин
    this.showLabel = function () {
        return this.name + ": " + this.carrying + " т";
    };

    // информация для отображения под списком машин,
    // при выборе строки в списке
    this.showInfo = function () {
        return "Тип: грузовая <br>" +
            "Модель: " + this.name + "<br>" +
            "Масса: " + this.carrying + " т. <br>" +
            "Примечание: " + this.description + "<br>";
    };
}
// 2.1. Наследование
Truck.prototype = Object.create(Vehicle.prototype);
Truck.prototype.constructor = Truck;

// 3. Конструктор Car
function Car(name, description, velocity) {
    this.velocity = velocity;
    Vehicle.apply(this, [name, description]);

    // информация для отображения в списке машин
    this.showLabel = function () {
        return this.name + ": " + this.velocity + " км/ч";
    };

    // информация для отображения под списком машин,
    // при выборе строки в списке
    this.showInfo = function () {
        return "Тип: легковая <br>" +
            "Модель: " + this.name + "<br>" +
            "Скорость: " + this.velocity + " км/ч <br>" +
            "Примечание: " + this.description + "<br>";
    };

}
// 3.1. Наследование
Car.prototype = Object.create(Vehicle.prototype);
Car.prototype.constructor = Car;


var form = document.getElementsByClassName('form')[0];
var button = document.getElementsByClassName('button')[0];
var list = document.getElementsByClassName('list')[0];
var data = document.getElementsByClassName('data')[0];

var vehicle = null;
var vehicles = [];

button.onclick  = function() {
    if (form.transport.value === 'light') {
        vehicle = new Car(form.model.value, form.desc.value, form.char.value);
    } else {
        vehicle = new Truck(form.model.value, form.desc.value, form.char.value);
    }

    vehicles.push({
        vehicle: vehicle,
        onClick: vehicle.showInfo.bind(vehicle)
    });

    list.innerHTML += "<option >"+vehicle.showLabel()+"</option>";

    vehicle = null;
};

